
#ifndef _bg2e_quicstart_event_handler_hpp_
#define _bg2e_quicstart_event_handler_hpp_

#include <bg/bg2e.hpp>

class MyEventHandler : public bg::base::EventHandler {
public:
	MyEventHandler();
	
	virtual void willCreateContext();

	virtual void buildMenu(bg::wnd::MenuDescriptor & menu);
	virtual void menuSelected(const std::string & title, int32_t identifier);
	
protected:
	virtual ~MyEventHandler();
};

#endif
