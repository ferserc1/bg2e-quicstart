function load() {
    class MyAppController extends bg.app.WindowController {
        init() {
            this.gl.clearColor(0.3,0.3,0.6,1.0);
        }

        display() {
            this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
            this.postRedisplay();
        }

        reshape(w,h) {
            this.gl.viewport(0,0,w,h);
        }
    }

    let controller = new MyAppController();
    let mainLoop = bg.app.MainLoop.singleton;

    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = document.getElementsByTagName('canvas')[0];
    mainLoop.run(controller);
}