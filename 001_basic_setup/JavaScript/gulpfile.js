const   gulp = require('gulp'),
        traceur = require('gulp-traceur'),
        concat = require('gulp-concat'),
        connect = require('gulp-connect'),
        replace = require('gulp-replace'),
        fs = require('fs'),
        path = require('path');

let config = {
    outDir: 'build',
    bg2eDir: `bower_components/bg2e-js/js/bg2e.js`, // Set here the path to compiled bg2e-js
    port: 8888
};

gulp.task("webserver", function() {
    connect.server({
        livereload: true,
        port: config.port,
        root: `${ config.outDir }`
    });
});

gulp.task("build", function() {
    gulp.src(`src/*.js`)
        .pipe(traceur())
        .pipe(concat("app.js"))
        .pipe(gulp.dest(`${ config.outDir }/js`));

    gulp.src('src/*.html')
        .pipe(gulp.dest(`${ config.outDir }`));

    gulp.src(`bower_components/traceur/traceur.min.js`)
        .pipe(gulp.dest(`${ config.outDir }/js`));

    gulp.src(config.bg2eDir)
        .pipe(gulp.dest(`${ config.outDir }/js`));
});

gulp.task("watch", function() {
    gulp.watch([
        "src/*.js",
        "src/*.html"
    ],["build"]);
});

gulp.task("default",["build","webserver","watch"]);
