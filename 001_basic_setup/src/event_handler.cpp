
#include <event_handler.hpp>


MyEventHandler::MyEventHandler()
{

}

MyEventHandler::~MyEventHandler() {
}

void MyEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else {
		throw new bg::base::InitException("Your platform does not support the required rendering engine.");
	}
}

void MyEventHandler::buildMenu(bg::wnd::MenuDescriptor & menu) {
	bg::wnd::PopUpMenu * file = bg::wnd::PopUpMenu::New("File");
	file->addMenuItem({
		bg::wnd::kCodeQuit,
		"Quit",
		{ bg::base::Keyboard::kCommandOrControlKey, bg::base::Keyboard::kKeyQ }
	});
	
	menu.push_back(file);
}

void MyEventHandler::menuSelected(const std::string & title, int32_t identifier) {
	if (identifier==bg::wnd::kCodeQuit) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}

